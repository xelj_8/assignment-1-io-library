section .text

%define DECIMAL_BASE 10

; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, 60
    syscall
    ret

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .loop:
        cmp byte[rax+rdi], 0
        je .end
        inc rax
        jmp .loop
    .end:
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    xor rax, rax
    push rdi
    call string_length
    pop rdi
    mov rdx, rax
    mov rsi, rdi
    mov rdi, 1
    mov rax, 1
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rsi, rsp
    pop rdi
    mov rdx, 1
    mov rax, 1
    mov rdi, 1
    syscall
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    xor rax, rax
    mov rdi, '/n'
    call print_char
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi
    push rbx
    mov r8, rsp
    mov rbx, DECIMAL_BASE
    push 0
    .loop:
        xor rdx, rdx
        div rbx
        add rdx, '0'
        dec rsp
        mov byte[rsp], dl
        test rax, rax
        je .end
        jmp .loop
    .end:
        mov rdi, rsp
        lea rsp, [r8 - 32]
        push r8
        call print_string
        pop r8
        mov rsp, r8
        pop rbx
        ret

; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
    xor rax, rax
    test rdi, rdi
    jge .print_pos
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi
    .print_pos:
        call print_uint
        ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
    xor rcx, rcx
    xor r8, r8
    xor r9, r9
    .loop:
        mov r8b, byte[rdi+rcx]
        mov r9b, byte[rsi+rcx]
        cmp r8b, r9b;
        jne .ret_f
        test r8b, r8b
        je .ret_t
        inc rcx
        jmp .loop
    .ret_t:
        mov rax, 1
    .ret_f:
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
    xor rdi, rdi
    mov rdx, 1
    push 0;
    mov rsi, rsp;
    syscall
    pop rax
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    xor rax, rax
    xor rcx, rcx
    .loop:
        push rcx
        push rsi
        push rdi
        call read_char
        pop rdi
        pop rsi
        pop rcx
        test rax, rax
        je .end
        cmp rax, ' '
        je .whitespace
        cmp rax, `\t`
        je .whitespace
        cmp rax, '\n'
        je .whitespace
        mov [rdi+rcx], rax
        inc rcx
        cmp rcx, rsi
        jl .loop
    xor rax, rax
    ret
    .whitespace:
        test rcx, rcx
        je .loop
    .end:
        mov byte[rdi+rcx], 0
        mov rax, rdi
        mov rdx, rcx
        ret


; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rcx, rcx
    push rbx
    mov rbx, DECIMAL_BASE
    xor r8, r8
    .loop:
        mov r8b, byte[rdi+rcx]
        test r8, r8
        je .end
        cmp r8b, '0'
        jl .end
        cmp r8b, '9'
        jg .end
        mul rbx
        sub r8b, '0'
        add rax, r8
        inc rcx
        jmp .loop
    .end:
        mov rdx, rcx
        pop rbx
        ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был)
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    xor rdx, rdx
    cmp byte[rdi], '-'
    jne .pos
    .neg:
        push rdi
        inc rdi
        call parse_uint
        neg rax
        inc rdx
        pop rdi
        ret
    .pos:
        call parse_uint
        ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
    push rsi
    push rdi
    call string_length
    pop rdi
    pop rsi
    cmp rdx, rax
    mov rcx, 0
    jge .loop
    xor rax, rax
    .loop:
        cmp rcx, rax
        jg .end
        mov r8, [rdi+rcx]
        mov [rsi+rcx], r8
        inc rcx
        jmp .loop
    .end:
        ret
